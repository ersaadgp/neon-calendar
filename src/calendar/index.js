import React, { useEffect, useState } from "react";
import moment from "moment";
import {
  days,
  months,
  generatePrev,
  generateMain,
  generateNext,
  helperChangeMonth,
} from "../components/helper";
import Modal from "../components/Modal";
import MonthsInput from "../components/Months";

const Calendar = () => {
  const dt = new Date();

  const [notes, setNotes] = useState();
  const [dateNotes, setDateNotes] = useState();
  const [dates, setDates] = useState();
  const [prevDates, setPrevDates] = useState();
  const [firstDate, setFirstDate] = useState();
  const [boards, setBoards] = useState([]);
  const [active, setActive] = useState();
  const [index, setIndex] = useState();
  const [show, setShow] = useState();
  const [current, setCurrent] = useState({
    month: dt.getMonth() + 1,
    year: dt.getFullYear(),
    full: `${dt.getFullYear()}-${dt.getMonth() + 1}`,
  });

  const daysInMonth = () => {
    const value = moment(current.full).daysInMonth();
    setDates(value);
  };

  const firstDayOfMonth = () => {
    let dateObject = moment(current.full);

    // days in prev months
    let prev = moment(current.full).add(-1, "M");
    prev = moment(prev).endOf("month").format("D");
    setPrevDates(prev);

    setFirstDate(moment(dateObject).startOf("month").format("d") - 1);
  };

  const generateBoard = () => {
    let boards = [];
    let prev = prevDates - firstDate;

    generatePrev(boards, prev, firstDate);
    generateMain(boards, firstDate, dates, active, handleClick, notes);
    generateNext(boards);
    setBoards(boards);
  };

  useEffect(() => {
    daysInMonth();
    firstDayOfMonth();
  }, [current]);

  useEffect(() => {
    generateBoard();
  }, [dates, active, notes, firstDate]);

  const handleChangeMonth = (type) => {
    let newDate = {};
    newDate = helperChangeMonth(type, current);
    setCurrent(newDate);
  };

  const handleClick = (i, j, e, label) => {
    e.stopPropagation();

    if (label) setShow(label);

    if (j < 3) {
      setActive(i);
      setIndex({ date: i, label: j });
      const modal = document.getElementById("myModal");
      modal.style.display = "flex";
    }
  };

  useEffect(() => {
    if (dateNotes) {
      const value = dateNotes.data.filter(
        (row) => row.month === current.month && row.year === current.year
      );
      setNotes(value);
    }
  }, [current, dateNotes]);

  useEffect(() => {
    const value = JSON.parse(localStorage.getItem("@dateNote"));
    setDateNotes(value);
  }, [localStorage]);

  return (
    <div className="container">
      <div className="calendar-container">
        <div className="box-title">
          <div className="btn prev" onClick={() => handleChangeMonth("prev")}>
            Prev
          </div>
          <div className="months">
            <MonthsInput current={current} setCurrent={setCurrent} />
          </div>
          <div className="btn next" onClick={() => handleChangeMonth("next")}>
            Next
          </div>
        </div>
        <div className="box-header">
          {days.map((row, i) => (
            <div className="item" key={i}>
              {row}
            </div>
          ))}
        </div>
        <div className="box-container">{boards}</div>
      </div>
      <Modal current={current} index={index} show={show} setShow={setShow} />
    </div>
  );
};

export default Calendar;
