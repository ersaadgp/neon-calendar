import logo from "./logo.svg";
import "./App.css";
import Calendar from "./calendar";
import { ThemeProvider, createTheme } from "@mui/material/styles";

const theme = createTheme({
  palette: {
    primary: {
      main: "#14ffec",
    },
    secondary: {
      main: "#0d7377",
    },
  },
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <Calendar />
      </div>
    </ThemeProvider>
  );
}

export default App;
