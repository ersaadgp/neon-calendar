import React, { useState, useEffect } from "react";
import { TextField, Button, Chip, Alert } from "@mui/material";
import "./modal.css";
import validator from "validator";
const dateNotes = JSON.parse(localStorage.getItem("@dateNote"));

const ModalComponent = ({ current, index, show, setShow }) => {
  const [name, setName] = useState("");
  const [time, setTime] = useState("");
  const [emails, setEmails] = useState([]);
  const [email, setEmail] = useState("");
  const [error, setError] = useState();

  const modal = document.getElementById("myModal");

  const handleClick = (i) => {
    clearForm();
    modal.style.display = "none";
  };

  window.onclick = (event) => {
    if (event.target == modal) {
      clearForm();
      modal.style.display = "none";
    }
  };

  const handleKeyUp = (e) => {
    const valid = !emails.find((row) => row === email);
    if (e.keyCode == 13 || e.keyCode == 32) {
      if (validator.isEmail(email) && valid) {
        setEmails([...emails, email]);
      } else if (!valid) {
        setError("Email written!");
      } else {
        setError("Email format not valid!");
      }
      setEmail("");
      setTimeout(() => {
        setError();
      }, 3000);
    }
  };

  const handleDelete = (row) => {
    let value = emails.filter((x) => x !== row);
    setEmails(value);
  };

  const handleSubmit = () => {
    if (!name) {
      setError("Name cannot be empty!");
      setTimeout(() => {
        setError();
      }, 3000);
      return;
    } else if (!time) {
      setError("Time cannot be empty!");
      setTimeout(() => {
        setError();
      }, 3000);
      return;
    }

    const id = `${current.month}-${current.year}-${index.date + 1}-${
      index.label
    }`;
    const color = Math.floor(Math.random() * 16777215).toString(16);

    let value = {
      id,
      name,
      time,
      emails,
      color: show ? show.color : color,
      month: current.month,
      year: current.year,
      date: index.date,
    };

    let notes = { data: [] };

    if (show) {
      notes = {
        data: dateNotes.data.map((row) => (row.id === show.id ? value : row)),
      };
    } else {
      if (dateNotes?.data?.length) notes = { data: [...dateNotes.data, value] };
      else notes = { data: [value] };
    }

    // set to local storage
    notes = JSON.stringify(notes);
    localStorage.setItem("@dateNote", notes);
    clearForm();
    window.location.reload();
  };

  const clearForm = () => {
    setName("");
    setTime("");
    setEmails([]);
    setEmail("");
    setShow();
    modal.style.display = "none";
  };

  useEffect(() => {
    if (show) {
      setName(show.name);
      setTime(show.time);
      setEmails(show.emails);
    }
  }, [show]);

  const handleDeleteEvent = () => {
    let notes = {
      data: dateNotes.data.filter((row) => row.id !== show.id),
    };

    // set to local storage
    notes = JSON.stringify(notes);
    localStorage.setItem("@dateNote", notes);
    clearForm();
    window.location.reload();
  };

  return (
    <div id="myModal" className="modal">
      <div className="modal-content">
        <div className="modal-header">
          <span className="close" onClick={handleClick}>
            &times;
          </span>
          <h2>Modal Header</h2>
        </div>
        <div className="modal-body">
          {error && <Alert severity="error">{error}</Alert>}
          <TextField
            label="Name"
            color="primary"
            fullWidth
            className="input"
            size="small"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <TextField
            label="Time"
            color="primary"
            fullWidth
            className="input"
            size="small"
            value={time}
            onChange={(e) => setTime(e.target.value)}
            type="time"
          />
          <TextField
            label="Guest"
            color="primary"
            fullWidth
            className="input"
            size="small"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            onKeyDown={handleKeyUp}
          />
          {emails.map((row, i) => (
            <Chip
              label={row}
              variant="outlined"
              color="primary"
              className="chip"
              onDelete={() => handleDelete(row)}
              key={i}
            />
          ))}
        </div>
        <div className="modal-footer">
          <Button
            color="primary"
            variant="contained"
            fullWidth
            onClick={handleSubmit}
          >
            Save
          </Button>
          {show && (
            <Button
              color="primary"
              variant="outlined"
              fullWidth
              onClick={handleDeleteEvent}
              style={{ marginTop: "10px" }}
            >
              Delete
            </Button>
          )}
        </div>
      </div>
    </div>
  );
};

export default ModalComponent;
