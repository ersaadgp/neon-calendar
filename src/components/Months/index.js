import React, { useState } from "react";
import { Popover, Grid, TextField, Button, Alert } from "@mui/material";
import { months } from "../helper";
import "./months.css";

export default function BasicPopover({ current, setCurrent }) {
  const [anchorMonth, setAnchorMonth] = useState(null);
  const [anchorYear, setAnchorYear] = useState(null);
  const [year, setYear] = useState("");
  const [error, setError] = useState();

  const handleClick = (event) => {
    setAnchorMonth(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorMonth(false);
  };

  const open = Boolean(anchorMonth);
  const id = open ? "simple-popover" : undefined;

  const handleClickYear = (event) => {
    setAnchorYear(event.currentTarget);
  };

  const handleCloseYear = () => {
    setAnchorYear(false);
  };

  const openYear = Boolean(anchorYear);
  const idYear = openYear ? "simple-popover" : undefined;

  const handleChangeMonth = (i) => {
    setCurrent({ ...current, month: i + 1, full: `${current.year}-${i + 1}` });
    handleClose();
  };

  const handleChangeYear = () => {
    if (!/\b\d{4}\b/g.test(year)) {
      setError("Invalid year format!");
      setTimeout(() => {
        setError();
      }, 3000);
      return;
    } else if (parseInt(year) < 2000 || parseInt(year) > 2100) {
      setError("Year range between 2000 - 2100!");
      setTimeout(() => {
        setError();
      }, 3000);
      return;
    }

    setCurrent({
      ...current,
      year: parseInt(year),
      full: `${year}-${current.month}`,
    });
    setAnchorYear(false);
  };

  const handleKeyUp = (e) => {
    if (e.keyCode == 13 || e.keyCode == 32) {
      handleChangeYear();
    }
  };

  return (
    <>
      <span
        onClick={handleClick}
        aria-describedby={id}
        style={{ marginRight: "10px" }}
      >
        {`${months[current.month - 1]}`}
      </span>
      <span
        onClick={handleClickYear}
        aria-describedby={idYear}
      >{`${current.year}`}</span>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorMonth}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <Grid container className="month-container" spacing={1}>
          {months.map((row, i) => (
            <Grid item xs={4} key={i}>
              <div className="month" onClick={() => handleChangeMonth(i)}>
                {row}
              </div>
            </Grid>
          ))}
        </Grid>
      </Popover>
      <Popover
        id={idYear}
        open={openYear}
        anchorEl={anchorYear}
        onClose={handleCloseYear}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <Grid container className="year-container" spacing={1}>
          {error && (
            <Grid item xs={12}>
              <Alert severity="error">{error}</Alert>
            </Grid>
          )}

          <Grid item xs={9}>
            <TextField
              label="Year"
              color="primary"
              fullWidth
              className="input"
              size="small"
              value={year}
              onChange={(e) => setYear(e.target.value)}
              onKeyDown={handleKeyUp}
              type="number"
              placeholder="Format: yyyy"
            />
          </Grid>
          <Grid item xs={3}>
            <Button
              variant="outlined"
              color="primary"
              onClick={handleChangeYear}
              fullWidth
              className="input"
            >
              Submit
            </Button>
          </Grid>
        </Grid>
      </Popover>
    </>
  );
}
