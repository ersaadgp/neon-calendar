export const days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

export const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

export const generatePrev = async (boards, prev, firstDate) => {
  for (let i = 0; i < firstDate; i++) {
    const component = (
      <div className="box-empty" key={i}>
        {prev + 1}
      </div>
    );
    boards.push(component);
    prev += 1;
  }
};

export const generateMain = async (
  boards,
  firstDate,
  dates,
  active,
  handleClick,
  notes
) => {
  for (let i = 0; i < dates; i++) {
    const data = notes?.filter((row) => row.date === i);
    const style = i === active ? "box active" : "box";
    const length = data?.length || 0;
    const component = (
      <div
        className={style}
        key={i + firstDate}
        onClick={(e) => handleClick(i, length, e)}
      >
        <div className="caption">{i + 1}</div>
        {data?.map((label, j) => (
          <div
            key={j}
            className="labels"
            onClick={(e) => handleClick(i, j, e, label)}
            style={{ backgroundColor: `#${label?.color}` }}
          >
            {label?.name}
          </div>
        ))}
      </div>
    );
    boards.push(component);
  }
};

export const generateNext = async (boards) => {
  const rest = Math.ceil(boards.length / 7) * 7 - boards.length;
  for (let i = 0; i < rest; i++) {
    const component = (
      <div className="box-empty" key={i + boards.length}>
        {i + 1}
      </div>
    );
    boards.push(component);
  }
};

export const helperChangeMonth = (type, current) => {
  let newDate = {};
  if (type === "prev") {
    const year = current.month > 1 ? current.year : current.year - 1;
    const month = current.month > 1 ? current.month - 1 : 12;
    return (newDate = {
      month: month,
      year: year,
      full: `${year}-${month}`,
    });
  } else {
    const year = current.month < 12 ? current.year : current.year + 1;
    const month = current.month < 12 ? current.month + 1 : 1;
    return (newDate = {
      month: month,
      year: year,
      full: `${year}-${month}`,
    });
  }
};
